<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202153146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE appartient (id INT AUTO_INCREMENT NOT NULL, vehicule_id INT DEFAULT NULL, usager_id INT DEFAULT NULL, INDEX IDX_4201BAA74A4A3511 (vehicule_id), INDEX IDX_4201BAA74F36F0FC (usager_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE capteur (id INT AUTO_INCREMENT NOT NULL, type_capteur VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE determine (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, parametre_id INT DEFAULT NULL, INDEX IDX_F46DBF28C54C8C93 (type_id), INDEX IDX_F46DBF286358FF62 (parametre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE infraction (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, opj_id INT DEFAULT NULL, capteur_id INT DEFAULT NULL, sanction_id INT DEFAULT NULL, vehicule_id INT DEFAULT NULL, usager_id INT DEFAULT NULL, horaire_infraction DATETIME NOT NULL, longitude DOUBLE PRECISION NOT NULL, photo_scene VARCHAR(50) NOT NULL, photo_usager VARCHAR(50) NOT NULL, procedure_infraction VARCHAR(50) NOT NULL, etat_dossier VARCHAR(50) NOT NULL, accepter_infraction VARCHAR(50) NOT NULL, issue_judiciaire VARCHAR(50) NOT NULL, latitude DOUBLE PRECISION NOT NULL, motif VARCHAR(255) DEFAULT NULL, INDEX IDX_C1A458F5C54C8C93 (type_id), INDEX IDX_C1A458F57C29F7A6 (opj_id), INDEX IDX_C1A458F51708A229 (capteur_id), INDEX IDX_C1A458F596E0C11A (sanction_id), INDEX IDX_C1A458F54A4A3511 (vehicule_id), INDEX IDX_C1A458F54F36F0FC (usager_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opj (id INT AUTO_INCREMENT NOT NULL, id_uti INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, disponible TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_3E92A4BF4CE2FEFD (id_uti), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parametre (id INT AUTO_INCREMENT NOT NULL, desc_parametre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE piece (id INT AUTO_INCREMENT NOT NULL, infraction_id INT DEFAULT NULL, date_emission DATETIME NOT NULL, date_enregistrement DATETIME NOT NULL, est_recu INT NOT NULL, photo_piece VARCHAR(50) NOT NULL, emetteur_piece VARCHAR(50) NOT NULL, INDEX IDX_44CA0B237697C467 (infraction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning (id INT AUTO_INCREMENT NOT NULL, date_deb DATE DEFAULT NULL, date_fin DATE DEFAULT NULL, validation SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning_opj (planning_id INT NOT NULL, opj_id INT NOT NULL, INDEX IDX_3FCB6A8D3D865311 (planning_id), INDEX IDX_3FCB6A8D7C29F7A6 (opj_id), PRIMARY KEY(planning_id, opj_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE possede (id INT AUTO_INCREMENT NOT NULL, capteur_id INT DEFAULT NULL, type_id INT DEFAULT NULL, INDEX IDX_3D0B15081708A229 (capteur_id), INDEX IDX_3D0B1508C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prends (id INT AUTO_INCREMENT NOT NULL, infraction_id INT DEFAULT NULL, parametre_id INT DEFAULT NULL, valeur VARCHAR(255) NOT NULL, INDEX IDX_BFDE70F37697C467 (infraction_id), INDEX IDX_BFDE70F36358FF62 (parametre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sanction (id INT AUTO_INCREMENT NOT NULL, point_retire INT NOT NULL, delit TINYINT(1) NOT NULL, amende INT NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, desc_type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usager (id INT AUTO_INCREMENT NOT NULL, id_uti INT DEFAULT NULL, id_permis VARCHAR(12) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, ville_usager VARCHAR(255) NOT NULL, pays_usager VARCHAR(255) NOT NULL, point_permis INT NOT NULL, UNIQUE INDEX UNIQ_3CDC65FF4CE2FEFD (id_uti), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur_site (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(50) NOT NULL, password VARCHAR(255) NOT NULL, role VARCHAR(50) NOT NULL, is_desactivate TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicule (id INT AUTO_INCREMENT NOT NULL, immatriculation VARCHAR(255) NOT NULL, marque VARCHAR(255) NOT NULL, modele VARCHAR(255) NOT NULL, couleur VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE appartient ADD CONSTRAINT FK_4201BAA74A4A3511 FOREIGN KEY (vehicule_id) REFERENCES vehicule (id)');
        $this->addSql('ALTER TABLE appartient ADD CONSTRAINT FK_4201BAA74F36F0FC FOREIGN KEY (usager_id) REFERENCES usager (id)');
        $this->addSql('ALTER TABLE determine ADD CONSTRAINT FK_F46DBF28C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE determine ADD CONSTRAINT FK_F46DBF286358FF62 FOREIGN KEY (parametre_id) REFERENCES parametre (id)');
        $this->addSql('ALTER TABLE infraction ADD CONSTRAINT FK_C1A458F5C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE infraction ADD CONSTRAINT FK_C1A458F57C29F7A6 FOREIGN KEY (opj_id) REFERENCES opj (id)');
        $this->addSql('ALTER TABLE infraction ADD CONSTRAINT FK_C1A458F51708A229 FOREIGN KEY (capteur_id) REFERENCES capteur (id)');
        $this->addSql('ALTER TABLE infraction ADD CONSTRAINT FK_C1A458F596E0C11A FOREIGN KEY (sanction_id) REFERENCES sanction (id)');
        $this->addSql('ALTER TABLE infraction ADD CONSTRAINT FK_C1A458F54A4A3511 FOREIGN KEY (vehicule_id) REFERENCES vehicule (id)');
        $this->addSql('ALTER TABLE infraction ADD CONSTRAINT FK_C1A458F54F36F0FC FOREIGN KEY (usager_id) REFERENCES usager (id)');
        $this->addSql('ALTER TABLE opj ADD CONSTRAINT FK_3E92A4BF4CE2FEFD FOREIGN KEY (id_uti) REFERENCES utilisateur_site (id)');
        $this->addSql('ALTER TABLE piece ADD CONSTRAINT FK_44CA0B237697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id)');
        $this->addSql('ALTER TABLE planning_opj ADD CONSTRAINT FK_3FCB6A8D3D865311 FOREIGN KEY (planning_id) REFERENCES planning (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE planning_opj ADD CONSTRAINT FK_3FCB6A8D7C29F7A6 FOREIGN KEY (opj_id) REFERENCES opj (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE possede ADD CONSTRAINT FK_3D0B15081708A229 FOREIGN KEY (capteur_id) REFERENCES capteur (id)');
        $this->addSql('ALTER TABLE possede ADD CONSTRAINT FK_3D0B1508C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE prends ADD CONSTRAINT FK_BFDE70F37697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id)');
        $this->addSql('ALTER TABLE prends ADD CONSTRAINT FK_BFDE70F36358FF62 FOREIGN KEY (parametre_id) REFERENCES parametre (id)');
        $this->addSql('ALTER TABLE usager ADD CONSTRAINT FK_3CDC65FF4CE2FEFD FOREIGN KEY (id_uti) REFERENCES utilisateur_site (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE infraction DROP FOREIGN KEY FK_C1A458F51708A229');
        $this->addSql('ALTER TABLE possede DROP FOREIGN KEY FK_3D0B15081708A229');
        $this->addSql('ALTER TABLE piece DROP FOREIGN KEY FK_44CA0B237697C467');
        $this->addSql('ALTER TABLE prends DROP FOREIGN KEY FK_BFDE70F37697C467');
        $this->addSql('ALTER TABLE infraction DROP FOREIGN KEY FK_C1A458F57C29F7A6');
        $this->addSql('ALTER TABLE planning_opj DROP FOREIGN KEY FK_3FCB6A8D7C29F7A6');
        $this->addSql('ALTER TABLE determine DROP FOREIGN KEY FK_F46DBF286358FF62');
        $this->addSql('ALTER TABLE prends DROP FOREIGN KEY FK_BFDE70F36358FF62');
        $this->addSql('ALTER TABLE planning_opj DROP FOREIGN KEY FK_3FCB6A8D3D865311');
        $this->addSql('ALTER TABLE infraction DROP FOREIGN KEY FK_C1A458F596E0C11A');
        $this->addSql('ALTER TABLE determine DROP FOREIGN KEY FK_F46DBF28C54C8C93');
        $this->addSql('ALTER TABLE infraction DROP FOREIGN KEY FK_C1A458F5C54C8C93');
        $this->addSql('ALTER TABLE possede DROP FOREIGN KEY FK_3D0B1508C54C8C93');
        $this->addSql('ALTER TABLE appartient DROP FOREIGN KEY FK_4201BAA74F36F0FC');
        $this->addSql('ALTER TABLE infraction DROP FOREIGN KEY FK_C1A458F54F36F0FC');
        $this->addSql('ALTER TABLE opj DROP FOREIGN KEY FK_3E92A4BF4CE2FEFD');
        $this->addSql('ALTER TABLE usager DROP FOREIGN KEY FK_3CDC65FF4CE2FEFD');
        $this->addSql('ALTER TABLE appartient DROP FOREIGN KEY FK_4201BAA74A4A3511');
        $this->addSql('ALTER TABLE infraction DROP FOREIGN KEY FK_C1A458F54A4A3511');
        $this->addSql('DROP TABLE appartient');
        $this->addSql('DROP TABLE capteur');
        $this->addSql('DROP TABLE determine');
        $this->addSql('DROP TABLE infraction');
        $this->addSql('DROP TABLE opj');
        $this->addSql('DROP TABLE parametre');
        $this->addSql('DROP TABLE piece');
        $this->addSql('DROP TABLE planning');
        $this->addSql('DROP TABLE planning_opj');
        $this->addSql('DROP TABLE possede');
        $this->addSql('DROP TABLE prends');
        $this->addSql('DROP TABLE sanction');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE usager');
        $this->addSql('DROP TABLE utilisateur_site');
        $this->addSql('DROP TABLE vehicule');
    }
}
