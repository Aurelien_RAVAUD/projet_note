<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210210142600 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE piece DROP FOREIGN KEY FK_44CA0B237697C467');
        $this->addSql('ALTER TABLE piece ADD CONSTRAINT FK_44CA0B237697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prends DROP FOREIGN KEY FK_BFDE70F37697C467');
        $this->addSql('ALTER TABLE prends ADD CONSTRAINT FK_BFDE70F37697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE piece DROP FOREIGN KEY FK_44CA0B237697C467');
        $this->addSql('ALTER TABLE piece ADD CONSTRAINT FK_44CA0B237697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id)');
        $this->addSql('ALTER TABLE prends DROP FOREIGN KEY FK_BFDE70F37697C467');
        $this->addSql('ALTER TABLE prends ADD CONSTRAINT FK_BFDE70F37697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id)');
    }
}
