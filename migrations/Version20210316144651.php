<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316144651 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etudiant ADD id INT AUTO_INCREMENT NOT NULL, CHANGE Id_ETUDIANT id_etudiant INT NOT NULL, CHANGE Prenom_ETUDIANT prenom_etudiant VARCHAR(255) NOT NULL, CHANGE Nom_ETUDIANT nom_etudiant VARCHAR(255) NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE piece DROP FOREIGN KEY FK_44CA0B237697C467');
        $this->addSql('ALTER TABLE piece ADD CONSTRAINT FK_44CA0B237697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prends DROP FOREIGN KEY FK_BFDE70F37697C467');
        $this->addSql('ALTER TABLE prends ADD CONSTRAINT FK_BFDE70F37697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE etudiant MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE etudiant DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE etudiant DROP id, CHANGE id_etudiant Id_ETUDIANT INT AUTO_INCREMENT NOT NULL, CHANGE prenom_etudiant Prenom_ETUDIANT TEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE nom_etudiant Nom_ETUDIANT TEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE etudiant ADD PRIMARY KEY (Id_ETUDIANT)');
        $this->addSql('ALTER TABLE piece DROP FOREIGN KEY FK_44CA0B237697C467');
        $this->addSql('ALTER TABLE piece ADD CONSTRAINT FK_44CA0B237697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id)');
        $this->addSql('ALTER TABLE prends DROP FOREIGN KEY FK_BFDE70F37697C467');
        $this->addSql('ALTER TABLE prends ADD CONSTRAINT FK_BFDE70F37697C467 FOREIGN KEY (infraction_id) REFERENCES infraction (id)');
    }
}
