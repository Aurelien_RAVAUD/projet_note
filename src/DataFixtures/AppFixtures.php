<?php

namespace App\DataFixtures;

use App\Entity\Appartient;
use App\Entity\Capteur;
use App\Entity\Determine;
use App\Entity\Infraction;
use App\Entity\Opj;
use App\Entity\Parametre;
use App\Entity\Piece;
use App\Entity\Planning;
use App\Entity\Possede;
use App\Entity\Prends;
use App\Entity\Sanction;
use App\Entity\Type;
use App\Entity\Usager;
use App\Entity\UtilisateurSite;
use App\Entity\Vehicule;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $planning = new Planning();
        $planning->setDateDeb(new DateTime('2021-01-06'));
        $planning->setDateFin(new DateTime('2021-02-06'));
        $planning->setValidation(1);
        
        $opj = new Opj();
        $opj->setNom("Brunet");
        $opj->setPrenom("Philippe");
        $opj->setDisponible(true);
        $opj->addPlanning($planning);
        $planning->addOpj($opj);
        
        $opj2 = new Opj();
        $opj2->setNom("Hermand");
        $opj2->setPrenom("Bernard");
        $opj2->setDisponible(false);
        $opj2->addPlanning($planning);
        $planning->addOpj($opj2);

        $opj3 = new Opj();
        $opj3->setNom("Bailly");
        $opj3->setPrenom("Jean");
        $opj3->setDisponible(true);
        $opj3->addPlanning($planning);
        $planning->addOpj($opj3);
        
        $user = new UtilisateurSite();
        $user->setLogin("admin@a.a");
        $user->setPassword('$2y$08$JF5ovQWQl4JdnnQ4laoJvumwEUD.FC2PvNBskGYBlPGCk/czBHMK.');
        $user->setRole("admin");
        $user->setIsDesactivate(false);

        $user2 = new UtilisateurSite();
        $user2->setLogin("opj@o.o");
        $user2->setPassword('$2y$08$uVsY3gEWzOLyMFntLSkLGO2CnSTe9nWjsHaygVnWZ4fZeEzlBfgVe');
        $user2->setRole("opj");
        $user2->setIsDesactivate(false);

        $user3 = new UtilisateurSite();
        $user3->setLogin("usager@u.u");
        $user3->setPassword('$2y$08$RfQnGMPt2D1GX7eA3zkfVu8jqJQAVFZ8Sm2cfD0EFNL8JrMrKR8Y2');
        $user3->setRole("usager");   
        $user3->setIsDesactivate(false);
        
        $user4 = new UtilisateurSite();
        $user4->setLogin("julien.leclerc30@orange.fr");
        $user4->setPassword('$2y$08$VkYFhBTuyv1bsJXCGszHUeOYrSY7GXm.TG3wLI9rW3.T9ndW9aupm');
        $user4->setRole("usager");
        $user4->setIsDesactivate(false);
        
        $user5 = new UtilisateurSite();
        $user5->setLogin("bertrand@renard.com");
        $user5->setPassword('$2y$08$5C2A6UNTHZxWwTMjiMshLeIY7AjO8tVCMBPOBLXZOT.114mcCyS6O');
        $user5->setRole("opj");
        $user5->setIsDesactivate(false);
        
        $user6 = new UtilisateurSite();
        $user6->setLogin("jack@boulon.com");
        $user6->setPassword(' $2y$08$4EtU07/SqKlJPl.NI3s64OGyIEXkcRoWsyOVZFMMfhhEeVV9k4k6K');
        $user6->setRole("admin");
        $user6->setIsDesactivate(false);
        
        $user7 = new UtilisateurSite();
        $user7->setLogin("florian.herault@orange.fr");
        $user7->setPassword('$2y$08$j3L2wnyaCbJ.SB4zMkD1sO5O0aUIMs1W/w7iz/NRDAanZpA.pH8Eu');
        $user7->setRole("usager");
        $user7->setIsDesactivate(false);
        
        
        $usager = new Usager();
        $usager->setNom("Albert")->setPrenom("Richard")->setCodePostal("86000")->setVilleUsager("Poitiers")->setPaysUsager("France")->setPointPermis(12)->setUtiId($user3);
        $usager->setIdPermis("123456789123");


        $opj->setUtiId($user2);
        
        $usager4 = new Usager();
        $usager4->setNom("Leclerc")->setPrenom("Julien")->setCodePostal("86000")->setVilleUsager("Poitiers")->setPaysUsager("France")->setPointPermis(12)->setUtiId($user4);
        $usager4->setIdPermis("753951486219");

        $usager5 = new Usager();
        $usager5->setNom("Bruel")->setPrenom("Patrick")->setCodePostal("86000")->setVilleUsager("Poitiers")->setPaysUsager("France")->setPointPermis(12);
        $usager5->setIdPermis("753951486213");

        
        $usager6 = new Usager();
        $usager6->setNom("Decrock")->setPrenom("Clement")->setCodePostal("86000")->setVilleUsager("Poitiers")->setPaysUsager("France")->setPointPermis(12)->setUtiId($user7);
        $usager6->setIdPermis("753951486218");

        $vehicule = new Vehicule();
        $vehicule->setImmatriculation("AA-111-AA");
        $vehicule->setMarque("Renault");
        $vehicule->setModele("Laguna");
        $vehicule->setCouleur("Gris");

        $vehicule2 = new Vehicule();
        $vehicule2->setImmatriculation("AG-194-BX");
        $vehicule2->setMarque("Peugeot");
        $vehicule2->setModele("206");
        $vehicule2->setCouleur("Rouge");

        $vehicule3 = new Vehicule();
        $vehicule3->setImmatriculation("AX-456-GQ");
        $vehicule3->setMarque("Citröen");
        $vehicule3->setModele("BX");
        $vehicule3->setCouleur("Verte");

        $appartenir = new Appartient();
        $appartenir->setVehicule($vehicule)->setUsager($usager);
        $usager->addAppartient($appartenir);

        $appartenir2 = new Appartient();
        $appartenir2->setVehicule($vehicule2)->setUsager($usager6);
        $usager6->addAppartient($appartenir2);

        $appartenir3 = new Appartient();
        $appartenir3->setVehicule($vehicule3)->setUsager($usager4);
        $usager4->addAppartient($appartenir3);

        $sanction = new Sanction();
        $infraction1 = new Infraction();
        $infraction2 = new Infraction();
        $infraction3 = new Infraction();
        $infraction4 = new Infraction();
        /*$infraction5 = new Infraction();
        $infraction6 = new Infraction();
        $infraction7 = new Infraction();
        $infraction8 = new Infraction();
        $infraction9 = new Infraction();
        $infraction10 = new Infraction();
        $infraction11 = new Infraction();
        $infraction13 = new Infraction();
        $infraction14 = new Infraction();
        $infraction15 = new Infraction();
        $infraction16 = new Infraction();
        $infraction17 = new Infraction();
        $infraction18 = new Infraction();
        $infraction19 = new Infraction();
        $infraction20 = new Infraction();*/

        $capteur = new Capteur();
        $parametre = new Parametre();
        $determine = new Determine();
        $type = new Type();
        $prends = new Prends();
        $possede = new Possede();

        $sanction2 = new Sanction();
        $capteur2 = new Capteur();
        $parametre2 = new Parametre();
        $determine2 = new Determine();
        $type2 = new Type();
        $prends2 = new Prends();
        $possede2 = new Possede();

        $sanction->setPointRetire(1);
        $sanction->setDelit(0);
        $sanction->setAmende(135);
        $sanction->setDescription("Excès de vitesse inférieur à 20 km/h (avec limitation inférieure ou égale à 50 km/h) ");
        
        $sanction2->setPointRetire(3);
        $sanction2->setDelit(1);
        $sanction2->setAmende(135);
        $sanction2->setDescription("Excès de vitesse égal ou supérieur à 30 km/h et inférieur à 40 km/h");
        
        $sanction3 = new Sanction();
        $sanction3->setPointRetire(1);
        $sanction3->setDelit(0);
        $sanction3->setAmende(68);
        $sanction3->setDescription("Excès de vitesse inférieur à 20 km/h (avec limitation supérieure à 50 km/h)");
        
        $sanction4 = new Sanction();
        $sanction4->setPointRetire(4);
        $sanction4->setDelit(0);
        $sanction4->setAmende(135);
        $sanction4->setDescription("Franchissement de feu rouge");
        
        $sanction5 = new Sanction();
        $sanction5->setPointRetire(6);
        $sanction5->setDelit(1);
        $sanction5->setAmende(1500);
        $sanction5 ->setDescription("Excès de vitesse supérieur ou égal à 50 km/h");
        

        $capteur->setTypeCapteur("type de capteur");
        $capteur2->setTypeCapteur("Radar de feu");
        $prends->setValeur("2");
        $prends2->setValeur("4");
        $parametre->setDescParametre("paramètre");
        $parametre2->setDescParametre("paramètre");
        $type->setDescType("Flash les véhicule dépassant la vitesse limitée");
        $type2->setDescType("Flash les véhicules grillant un feu rouge");

        $capteur->addPossede($possede);
        $capteur2->addPossede($possede2);
        $parametre->addPrend($prends);
        $parametre2->addPrend($prends2);
        $parametre->addDetermine($determine);
        $parametre2->addDetermine($determine2);
        $type->addDetermine($determine);
        $type2->addDetermine($determine2);
        $type->addPossede($possede);
        $type2->addPossede($possede2);

        $possede->setCapteur($capteur);
        $possede2->setCapteur($capteur2);

        $infraction1->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("")->setUsager($usager);
        $infraction2->setHoraireInfraction(new DateTime('NOW'))->setLongitude(4.855389)->setLatitude(45.749713)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction)->setVehicule($vehicule2)->setAccepterInfraction("")->setIssueJudiciaire("")->setUsager($usager4);
        $infraction3->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.344819)->setLatitude( 46.581101)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction2)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("")->setUsager($usager4);
        $infraction4->setHoraireInfraction(new DateTime('NOW'))->setLongitude(1.308709)->setLatitude(43.862908)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction3)->setVehicule($vehicule2)->setAccepterInfraction("")->setIssueJudiciaire("")->setUsager($usager6);
        /*$infraction5->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction4)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("")->setUsager($usager4);
        $infraction6->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction5)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("")->setUsager($usager4);
        $infraction7->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction2)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("")->setUsager($usager5);
        $infraction8->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction3)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction9->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj)->setCapteur($capteur)->setSanction($sanction2)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction10->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj2)->setCapteur($capteur)->setSanction($sanction2)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction11->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj2)->setCapteur($capteur)->setSanction($sanction3)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction13->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj2)->setCapteur($capteur)->setSanction($sanction3)->setVehicule($vehicule3)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction14->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj2)->setCapteur($capteur)->setSanction($sanction)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction15->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj2)->setCapteur($capteur)->setSanction($sanction4)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction16->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj2)->setCapteur($capteur)->setSanction($sanction4)->setVehicule($vehicule3)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction17->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj3)->setCapteur($capteur)->setSanction($sanction4)->setVehicule($vehicule2)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction18->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj3)->setCapteur($capteur)->setSanction($sanction4)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction19->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj3)->setCapteur($capteur)->setSanction($sanction4)->setVehicule($vehicule3)->setAccepterInfraction("")->setIssueJudiciaire("");
        $infraction20->setHoraireInfraction(new DateTime('NOW'))->setLongitude(0.21)->setLatitude(46.37)->setPhotoScene("images/photoscene.png")->setPhotoUsager("images/photousager.png")->setProcedureInfraction("")->setEtatDossier("à traiter")->setType($type)->addPrend($prends)->setOpj($opj3)->setCapteur($capteur)->setSanction($sanction5)->setVehicule($vehicule)->setAccepterInfraction("")->setIssueJudiciaire("");*/
        $prends->setInfraction($infraction1);
        $prends->setInfraction($infraction2);
        $prends->setInfraction($infraction3);
        $prends->setInfraction($infraction4);
        /* $prends->setInfraction($infraction5);
         $prends->setInfraction($infraction6);
         $prends->setInfraction($infraction7);
         $prends->setInfraction($infraction8);
         $prends->setInfraction($infraction9);
         $prends2->setInfraction($infraction10);
         $prends2->setInfraction($infraction11);
         $prends2->setInfraction($infraction13);
         $prends2->setInfraction($infraction14);
         $prends2->setInfraction($infraction15);
         $prends2->setInfraction($infraction16);
         $prends2->setInfraction($infraction17);
         $prends2->setInfraction($infraction18);
         $prends2->setInfraction($infraction19);
         $prends2->setInfraction($infraction20);*/
        $prends->setParametre($parametre);
        $prends2->setParametre($parametre2);
        $determine->setParametre($parametre);
        $determine2->setParametre($parametre2);
        $determine->setType($type);
        $determine2->setType($type2);
        $possede->setType($type);
        $possede2->setType($type2);

        $piece = new Piece();
        $piece2 = new Piece();
        $piece->setDateEmission(new DateTime('NOW'));
        $piece->setDateEnregistrement(new DateTime('NOW'));
        $piece->setEstRecu(1);
        $piece->setPhotoPiece(" ");
        $piece->setEmetteurPiece($opj->getPrenom());
        $piece->setInfraction($infraction1);

        $piece2->setDateEmission(new DateTime('NOW'));
        $piece2->setDateEnregistrement(new DateTime('NOW'));
        $piece2->setEstRecu(1);
        $piece2->setPhotoPiece(" ");
        $piece2->setEmetteurPiece($opj->getPrenom());
        $piece2->setInfraction($infraction2);

        $manager->persist($planning);
        $manager->persist($opj);
        $manager->persist($opj2);
        $manager->persist($opj3);
        $manager->persist($user);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($user4);
        $manager->persist($user5);
        $manager->persist($user6);
        $manager->persist($user7);
        $manager->persist($usager);
        $manager->persist($usager4);
        $manager->persist($usager5);
        $manager->persist($usager6);
        $manager->persist($vehicule);
        $manager->persist($vehicule2);
        $manager->persist($vehicule3);
        $manager->persist($appartenir);
        $manager->persist($appartenir2);
        $manager->persist($appartenir3);
        $manager->persist($sanction);
        $manager->persist($infraction1);
        $manager->persist($infraction2);
        $manager->persist($infraction3);
        $manager->persist($infraction4);
        /*$manager->persist($infraction5);
        $manager->persist($infraction6);
        $manager->persist($infraction7);
        $manager->persist($infraction8);
        $manager->persist($infraction9);
        $manager->persist($infraction10);
        $manager->persist($infraction11);
        $manager->persist($infraction13);
        $manager->persist($infraction14);
        $manager->persist($infraction15);
        $manager->persist($infraction16);
        $manager->persist($infraction17);
        $manager->persist($infraction18);
        $manager->persist($infraction19);
        $manager->persist($infraction20);*/
        $manager->persist($capteur);
        $manager->persist($parametre);
        $manager->persist($determine);
        $manager->persist($type);
        $manager->persist($prends);
        $manager->persist($possede);
        $manager->persist($piece);
        $manager->persist($sanction2);
        $manager->persist($sanction3);
        $manager->persist($sanction4);
        $manager->persist($sanction5);
        $manager->persist($capteur2);
        $manager->persist($parametre2);
        $manager->persist($determine2);
        $manager->persist($type2);
        $manager->persist($prends2);
        $manager->persist($possede2);
        $manager->persist($piece2);
        $manager->flush();
    }
}
