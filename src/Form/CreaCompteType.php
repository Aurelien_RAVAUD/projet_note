<?php

namespace App\Form;

use App\Entity\UtilisateurSite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreaCompteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('password',PasswordType::class)
            ->add('verifPassword',PasswordType::class)
//            ->add('nom',TextType::class)
//            ->add('prenom',TextType::class)
            ->add('code_postal',TextType::class)
            ->add('ville_usager',TextType::class)
            ->add('pays_usager',TextType::class)
            ->add('role', ChoiceType::class, [
                'choices' => [
                    'Admin' => 'admin',
                    'OPJ' => 'opj',
                    'Usager' => 'usager',
                    'Mention' => 'mention',
                    'Rannee'=>'rannee'
                ],
                'preferred_choices' => ['usager', 'arr'],
            ])

            ->add('inscription', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            
        ]);
    }
}
