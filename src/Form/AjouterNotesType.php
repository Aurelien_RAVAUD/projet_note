<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AjouterNotesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $etudiant= $options['listEtudiant'];
        $ue= $options['listUe'];

        $builder

//            ->add('idnote', TextType::class, array('attr' => array('placeholder' => 'Note Id', 'class' => 'capteur_info')))
            ->add('note', TextType::class, array('attr' => array('placeholder' => 'Note', 'class' => 'capteur_info')))
            ->add('coeffnote', TextType::class, array('attr' => array('placeholder' => 'Coeff Note', 'class' => 'capteur_info')))
            ->add('idue', TextType::class, array('attr' => array('placeholder' => 'Id UE', 'class' => 'capteur_info')))
            ->add('idetudiant', TextType::class, array('attr' => array('placeholder' => 'Id Etudiant', 'class' => 'capteur_info')))




//                        ->add('etudiant', ChoiceType::class, ['choices' => $etudiant,
//                'label' => "Choisissez UN ETUDIANT",
//                'required' => true])
//
//            ->add('ue', ChoiceType::class, ['choices' => $ue,
//                'label' => "Choisissez une UE",
//                'required' => true])
            ->add('enregistrer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

            'listEtudiant'=>1,
            'listUe'=>1,
        ]);
    }
}