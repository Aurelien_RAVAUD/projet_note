<?php

namespace App\Form;

use App\Entity\UtilisateurSite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SecurityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('login', EmailType::class, array('attr' => array('placeholder' => 'Compte', 'class' => 'fadeIn second')))
        ->add('password',PasswordType::class, array('attr' => array('placeholder' => 'Mot de passe', 'class' => 'fadeIn third')))
        ->add('connexion', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UtilisateurSite::class,
        ]);
    }
}
