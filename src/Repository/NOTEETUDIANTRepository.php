<?php

namespace App\Repository;

use App\Entity\NOTEETUDIANT;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NOTEETUDIANT|null find($id, $lockMode = null, $lockVersion = null)
 * @method NOTEETUDIANT|null findOneBy(array $criteria, array $orderBy = null)
 * @method NOTEETUDIANT[]    findAll()
 * @method NOTEETUDIANT[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NOTEETUDIANTRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NOTEETUDIANT::class);
    }

    // /**
    //  * @return NOTEETUDIANT[] Returns an array of NOTEETUDIANT objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NOTEETUDIANT
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
