<?php

namespace App\Repository;

use App\Entity\UtilisateurSite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UtilisateurSite|null find($id, $lockMode = null, $lockVersion = null)
 * @method UtilisateurSite|null findOneBy(array $criteria, array $orderBy = null)
 * @method UtilisateurSite[]    findAll()
 * @method UtilisateurSite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurSiteRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, UtilisateurSite::class);
    }

    // /**
    //  * @return UtilisateurSite[] Returns an array of UtilisateurSite objects
    //  */
    /*
      public function findUsagerByUtilSiteById($id)
      {
      return $this->createQueryBuilder('u')
      ->andWhere('u.id = :idUtilSite')
      ->setParameter('idUtilSite', $id)
      ->leftJoin('u.usa_id', 'usager')
      ->addSelect('usager')
      ->andWhere('usager = :idUsager')
      ->setParameter('idUsager', $id)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      } */

    public function findByLogin($login)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.login = :login')
            ->setParameter('login', $login)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /*
      public function findOneBySomeField($value): ?UtilisateurSite
      {
      return $this->createQueryBuilder('u')
      ->andWhere('u.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
