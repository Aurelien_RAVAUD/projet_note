<?php


namespace App\Controller;


use App\Form\AjouterNotesType;
use App\Form\AjouterNotesUeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\DonneesCapteurType;
use App\Entity\Vehicule;
use App\Entity\Infraction;
use App\Entity\Note;
use App\Entity\Etudiant;
use App\Entity\Usager;
use App\Entity\Sanction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Type;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AjouterController extends AbstractController
{
    /**
     * @Route("/ajouternotes", name="ajouter_notes")
     */
    public function DonneesC(Request $req, EntityManagerInterface $em,AuthenticationUtils $authenticationUtils) {

        $entityManager = $this->getDoctrine()->getManager();
        $bool = false;
        $listEtudiant2 = $entityManager->getRepository('\App\Entity\Etudiant')->findAll();
        $listEtudiant =  array();
        foreach ($listEtudiant2 as $etudiant){
            $listEtudiant[$etudiant->getNomETUDIANT()." ".$etudiant->getPrenomETUDIANT()]= $etudiant->getId();

        }
        $listUe2 = $entityManager->getRepository('\App\Entity\Ue')->findAll();
        $listUe =  array();
        foreach ($listUe2 as $ue){
            $listUe[$ue->getNomUE()]= $ue->getIdUE();
        }

        $form = $this->createForm(AjouterNotesType::class, null, [
            'method' => 'POST',
            'action' => '/ajouternotes2',
            'listEtudiant'=>$listEtudiant,
            'listUe'=>$listUe,

        ]);

        $response = $this->render('ajouter/ajouter_notes.html.twig', [
            'formAjouterNotes' => $form->createView() ,'etudiants'=>$listEtudiant2,'ues'=>$listUe2, 'estInserer' => $bool,
        ]);

        return $response;
    }

    /**
     * @Route("ajouternotes2", name="create_noteAAAAAAAAAAAA")
     */
    public function AjoutDonneesC(Request $req, EntityManagerInterface $em) {
        //dump($req);
        $entityManager = $this->getDoctrine()->getManager();

        $listEtudiant2 = $entityManager->getRepository('\App\Entity\Etudiant')->findAll();
        $listEtudiant =  array();
        foreach ($listEtudiant2 as $etudiant){
            $listEtudiant[$etudiant->getNomETUDIANT()." ".$etudiant->getPrenomETUDIANT()]= $etudiant->getId();

        }
        $listUe2 = $entityManager->getRepository('\App\Entity\Ue')->findAll();
        $listUe =  array();
        foreach ($listUe2 as $ue){
            $listUe[$ue->getNomUE()]= $ue->getIdUE();
        }

        $form = $this->createForm(AjouterNotesType::class, null, [
            'method' => 'POST',
            'action' => '/ajouternotes2',
            'listEtudiant'=>$listEtudiant,
            'listUe'=>$listUe,

        ]);

        if ($req) {
            $dataNote = $req->request->get('ajouter_notes');
//           $idnote = $dataNote["idnote"];
            $note1 = $dataNote["note"];
            $coeffnote = $dataNote["coeffnote"];
            $idue = $dataNote["idue"];
            $idetudiant = $dataNote["idetudiant"];


//            $etudiant = $em->getRepository('\App\Entity\Etudiant')->find($dataNote["etudiant"]);
            $entityManager = $this->getDoctrine()->getManager();
            $note= new Note();
            //$note->setIdNOTE();
            $note->setNoteNOTE($note1);
            $note->setTestnoteNOTE($idetudiant);
            $note->setIdUE($idue);
            $note->setCoeffNOTE($coeffnote);

            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($note);

            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();

            $entityManager = $this->getDoctrine()->getManager();
//    POURN FAIRE AVEC LISTE DEROULANTE$type = $entityManager->getRepository('\App\Entity\Type')->find($typeId);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($note);
            $entityManager->flush();

            $bool = true;
        } else {

            $bool = false;
        }

//        $response = $this->render('ajouter/ajouter_notes_message.html.twig', [
//            'estInserer' => $bool,
        $response = $this->render('ajouter/ajouter_notes.html.twig', [
            'formAjouterNotes' => $form->createView() ,'etudiants'=>$listEtudiant2,'ues'=>$listUe2 , 'estInserer' => $bool,
        ]);

        $this->addFlash('success','Article ajouté beau goss');
        return $response;
    }

    /**
     * @Route("/ajouternotesue/{id}", name="ajouter_notes_ue")
     */
    public function DonneesD(Request $req,$id, EntityManagerInterface $em,AuthenticationUtils $authenticationUtils) {
        $entityManager = $this->getDoctrine()->getManager();

        $ues = $em->getRepository('App:Ue')->findAll();
        $listeUe = array();
        $listEtudiant2 = $entityManager->getRepository('\App\Entity\Etudiant')->findAll();
        $listEtudiant =  array();

        $uesetus = $entityManager->getRepository('\App\Entity\LiaisonUeEtudiant')->findAll();
        $listeUeEtudiant =  array();

        $etudiants = $entityManager->getRepository('\App\Entity\Etudiant')->findAll();
        $compteEtudiant =  array();

        foreach ($uesetus as $ueetu) {
//            if ($ueetu->getIdUE() == $id) {
                array_push($listeUeEtudiant, $ueetu);
        }

        foreach ($listEtudiant2 as $etudiant){
            $listEtudiant[$etudiant->getNomETUDIANT()." ".$etudiant->getPrenomETUDIANT()]= $etudiant->getId();
        }

        foreach($etudiant as $etudiant) {
                array_push($compteEtudiant, $etudiant);
            }

        foreach($ues as $ue) {
            if ($ue->getIdUE() == $id) {
                array_push($listeUe, $ue);
            }
        }
        $form = $this->createForm(AjouterNotesUeType::class, null, [
            'method' => 'POST',
            'action' => '/ajouternotesUE',
            'listEtudiant'=>$listEtudiant,
        ]);
        $response = $this->render('ajouter/ajouter_notes_ue.html.twig', [
            'formAjouterNotes' => $form->createView() ,'etudiants'=>$listEtudiant2,'ues'=>$listeUe,'uesetus'=>$listeUeEtudiant
        ]);

        return $response;
    }
//Méthode de remplissage formulaire
    /**
     * @Route("ajouternotesUE", name="create_noteUE")
     */
    public function AjoutDonneesCUE(Request $req, EntityManagerInterface $em) {
        //dump($req);

//        $id=1;
        $uesetus = $em->getRepository('\App\Entity\LiaisonUeEtudiant')->findAll();
        $listeUeEtudiant =  array();

        $ue = $em->getRepository('\App\Entity\Ue')->findAll();
        $listeUeEtudiant =  array();

//        foreach ($uesetus as $ueetu) {
//            if ($ueetu->getIdUE() == $id) {
//                array_push($listeUeEtudiant, $ueetu);
//            }
//        }
//        $id=$uesetus->getIdUe();
        if ($req) {
            $dataNote = $req->request->get('ajouter_notes_ue');
//           $idnote = $dataNote["idnote"];
            $note1 = $dataNote["note"];
            $coeffnote = $dataNote["coeffnote"];
//            $idue = $dataNote["idue"];
            $idetudiant = $dataNote["idetudiant"];
            $idue = $ue->getIdUe();


//            $etudiant = $em->getRepository('\App\Entity\Etudiant')->find($dataNote["etudiant"]);
            $entityManager = $this->getDoctrine()->getManager();
            $note= new Note();
            //$note->setIdNOTE();
            $note->setNoteNOTE($note1);
            $note->setTestnoteNOTE($idetudiant);
            $note->setIdUE($idue);
            $note->setCoeffNOTE($coeffnote);

            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($note);

            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();

            $entityManager = $this->getDoctrine()->getManager();
//    POURN FAIRE AVEC LISTE DEROULANTE$type = $entityManager->getRepository('\App\Entity\Type')->find($typeId);

            $entityManager = $this->getDoctrine()->getManager();

            // tell Doctrine you want to (eventually) save the Product (no queries yet)
            $entityManager->persist($note);

            // actually executes the queries (i.e. the INSERT query)
            $entityManager->flush();

            $bool = true;
        } else {

            $bool = false;
        }

        $response = $this->render('ajouter/ajouter_notes_message.html.twig', [
            'estInserer' => $bool,
        ]);

        return $response;
    }

    //Liste des UE
    /**
     * @Route("/ajouternoteslisteue", name="ajouter_notes_listeue")
     */
    public function ajouter_notes_listeue(EntityManagerInterface $em )
    {
        $ues = $em->getRepository('App:Ue')->findAll();
        $listeUe = array();

        foreach($ues as $ue){


            array_push($listeUe,$ue);

        }

        return $this->render('ajouter/ajouter_notes_listeue.html.twig',['ues'=>$listeUe]);
    }
}