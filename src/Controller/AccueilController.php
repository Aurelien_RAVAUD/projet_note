<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\LoginType;


class AccueilController extends AbstractController
{

    /**
     * @Route("/", name="accueil")
     */
    public function Accueil(Request $req)
    {
        return $this->render('accueil/accueil.html.twig');
    }

    /**
     * @return Response
     * @Route("/consult/etudiant",
     * name="retour1")
     */
    public function retouretudiant(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:Etudiant')->findAll();
        $compteEtu = array();

        foreach($comptes as $compte){

//            if($compte->getRole() == "opj" && $compte->getIsDesactivate() == false){
            array_push($compteEtu,$compte);
//            }
        }

        return $this->render('gestion_compte/viewtest.html.twig',['comptes'=>$compteEtu]);
    }

}


