<?php

namespace App\Controller;

use App\Entity\Opj;
use App\Entity\Usager;
use App\Entity\UtilisateurSite;
use App\Form\CreaCompteType;
use App\Form\LoginType;
use App\Form\ModifCompteType;
use App\Form\OpjType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreaCompteController extends AbstractController
{
    /**
     * @Route("/creaCompteMention", name="crea_compteMention")
     */
    public function CreaCompteMention(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = new UtilisateurSite();
        $uti->setRole("mention");
        $form = $this->createForm(ModifCompteType::class, $uti);
        $uti->setIsDesactivate(false);
        $form->add('inscription', SubmitType::class);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $dataUti = $req->request->get('modif_compte');
            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {
                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);
                $uti->setPassword($encoded);
                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();
                $ret = $this->redirectToRoute('gestion_compte');
            } else {
                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }
        } else {
            return $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }
        return $ret;
    }
    /**
     * @Route("/creaCompteParcours", name="crea_compteParcours")
     */
    public function CreaCompteParcours(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = new UtilisateurSite();
        $uti->setRole("parcours");
        $form = $this->createForm(ModifCompteType::class, $uti);
        $uti->setIsDesactivate(false);
        $form->add('inscription', SubmitType::class);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $dataUti = $req->request->get('modif_compte');
            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {
                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);
                $uti->setPassword($encoded);
                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();
                $ret = $this->redirectToRoute('gestion_compte');
            } else {
                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }
        } else {
            return $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }
        return $ret;
    }
    /**
     * @Route("/creaCompteResponsableannee", name="crea_compteRannee")
     */
    public function CreaCompteRannee(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = new UtilisateurSite();
        $uti->setRole("rannee");
        $form = $this->createForm(ModifCompteType::class, $uti);
        $uti->setIsDesactivate(false);
        $form->add('inscription', SubmitType::class);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $dataUti = $req->request->get('modif_compte');
            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {
                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);
                $uti->setPassword($encoded);
                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();
                $ret = $this->redirectToRoute('gestion_compte');
            } else {
                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }
        } else {
            return $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }
        return $ret;
    }
    /**
     * @Route("/creaCompteEnseignantdannee", name="crea_compteEannee")
     */
    public function CreaCompteEannee(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = new UtilisateurSite();
        $uti->setRole("eannee");
        $form = $this->createForm(ModifCompteType::class, $uti);
        $uti->setIsDesactivate(false);
        $form->add('inscription', SubmitType::class);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $dataUti = $req->request->get('modif_compte');
            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {
                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);
                $uti->setPassword($encoded);
                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();
                $ret = $this->redirectToRoute('gestion_compte');
            } else {
                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }
        } else {
            return $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }
        return $ret;
    }
    /**
     * @Route("/creaCompteUe", name="crea_compteUe")
     */
    public function CreaCompteUe(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = new UtilisateurSite();
        $uti->setRole("ue");
        $form = $this->createForm(ModifCompteType::class, $uti);
        $uti->setIsDesactivate(false);
        $form->add('inscription', SubmitType::class);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $dataUti = $req->request->get('modif_compte');
            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {
                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);
                $uti->setPassword($encoded);
                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();
                $ret = $this->redirectToRoute('gestion_compte');
            } else {
                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }
        } else {
            return $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }
        return $ret;
    }


    /**
     * @Route("/creaCompte", name="crea_compte")
     */
    public function CreaCompte(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {


        $uti = new UtilisateurSite();
        $form = $this->createForm(ModifCompteType::class);
        $uti->setRole("usager");
        $form->add('numero_neph',TextType::class, array('attr' => array('placeholder' => 'NEPH','maxlength' => 12)));

        $uti->setIsDesactivate(false);
        $form->add('inscription', SubmitType::class);

        $form->handleRequest($req);
        dump($form);
        if ($form->isSubmitted() && $form->isValid()) {

            $dataUti = $req->request->get('modif_compte');

            $usa = $em->getRepository("App:Usager")->findUsagerByIdPermis($dataUti["numero_neph"]);

            if(!$usa) {
                throw new NotFoundHttpException("L'usager n'existe pas");
            }

            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {

                $uti->setLogin($dataUti["login"]);
                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);

                $uti->setPassword($encoded);

                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();

                $usa->setUtiId($uti);
                $man->persist($usa);
                $man->flush();

                $ret = $this->redirectToRoute('login');

            } else {

                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }

        } else {

            return $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }

        return $ret;

    }

    /**
     * @Route("/creaCompteOpj", name="crea_compteOpj")
     */
    public function CreaCompteOpj(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = new UtilisateurSite();
        $opj = new Opj();
        $form = $this->createForm(ModifCompteType::class);
        $uti->setRole("mention");

        $form->add('mention', OpjType::class);

        $uti->setIsDesactivate(false);

        $form->add('creation', SubmitType::class);

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $dataUti = $req->request->get('modif_compte');
            $dataOpj = $dataUti["mention"];

            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {

                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);
                $uti->setLogin($dataUti["login"]);
                $uti->setPassword($encoded);

                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();

                $opj->setNom($dataOpj["nom"]);
                $opj->setPrenom($dataOpj["prenom"]);
                $opj->setDisponible(true);
                $opj->setUtiId($uti);
                $man->persist($opj);
                $man->flush();

                $ret = $this->redirectToRoute('gestion_compte');

            }else {

                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }

        }else{
            $ret = $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }

        return $ret;

    }

    /**
     * @Route("/creaCompteAdmin", name="crea_compteAdmin")
     */
    public function CreaCompteAdmin(UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = new UtilisateurSite();

        $uti->setRole("admin");
        $form = $this->createForm(ModifCompteType::class, $uti);

        $uti->setIsDesactivate(false);
        $form->add('inscription', SubmitType::class);

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $dataUti = $req->request->get('modif_compte');

            if ($dataUti["password"]["first"] == $dataUti["password"]["second"]) {

                $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);

                $uti->setPassword($encoded);

                $doc = $this->getDoctrine();
                $man = $doc->getManager();
                $man->persist($uti);
                $man->flush();

                $ret = $this->redirectToRoute('gestion_compte');

            } else {

                $ret = $this->render('crea_compte/index.html.twig', [
                    'formCreaCompte' => $form->createView(),
                ]);
            }

        } else {

            return $this->render('crea_compte/index.html.twig', [
                'formCreaCompte' => $form->createView(),
            ]);
        }

        return $ret;

    }


}
