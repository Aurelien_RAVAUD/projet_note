<?php

namespace App\Controller;

use App\Entity\UtilisateurSite;
use App\Form\LoginType;
use App\Form\OpjType;
use App\Form\UsagerType;
use App\Entity\Usager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ModifCompteType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ModifCompteController extends AbstractController
{
    /**
     * @Route("/modifCompte/{id}", name="modifCompte")
     */
    public function ModifCompte($id, UserPasswordEncoderInterface $encode, Request $req, EntityManagerInterface $em)
    {
        $uti = $em->getRepository('App:UtilisateurSite')->find($id);
        if ($this->getUser()->getRole() == "admin") {

        } else if ($id != $this->getUser()->getId()) {
            throw new AccessDeniedException("Compte non trouvé");
        }

        $form = $this->createForm(LoginType::class);

        $form->add('Modifier', SubmitType::class);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataUti = $req->request->get('login');

            if ($encode->isPasswordValid($uti, $dataUti["password"])) {

                if ($dataUti["newpassword"]["first"] == $dataUti["newpassword"]["second"]) {

                    $dataUti = $req->request->get('login');
                    $encoded = $encode->encodePassword($uti, $dataUti["newpassword"]["first"]);
                    $uti->setPassword($encoded);
                    $em->persist($uti);
                    $em->flush();

                    return $this->redirectToRoute('login');
                }else {
                    return $this->render('modif_compte/index.html.twig', [
                        'formModifCompte' => $form->createView(),
                        'utilisateur' => $uti,
                    ]);
                }
            } else {
                return $this->render('modif_compte/index.html.twig', [
                    'formModifCompte' => $form->createView(),
                    'utilisateur' => $uti,
                ]);
            }


        } else {
            return $this->render('modif_compte/index.html.twig', [
                'formModifCompte' => $form->createView(),
                'utilisateur' => $uti,
            ]);
        }
    }
}