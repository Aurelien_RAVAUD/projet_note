<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\SecurityType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $req)
    {
        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();

        $dataLogin = $req->request->get('security');
        $form = $this->createForm(SecurityType::class, null, [
            'method' => 'POST',
        ]);

        return $this->render('security/index.html.twig', [
            'formLogin' => $form->createView(),
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }
}
