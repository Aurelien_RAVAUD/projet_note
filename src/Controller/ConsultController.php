<?php


namespace App\Controller;


use App\Form\GetLinkForRestType;
use App\Form\ResetPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Note;
use App\Entity\Etudiant;
use App\Repository\NoteRepository;


class ConsultController extends AbstractController
{

    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/consult/etudiants",
     * name="consult_etudiant")
     */
    public function consult_etudiant(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:Etudiant')->findAll();
        $compteEtu = array();

        foreach($comptes as $compte){

//            if($compte->getRole() == "opj" && $compte->getIsDesactivate() == false){
            array_push($compteEtu,$compte);
//            }
        }

        return $this->render('gestion_compte/viewtest.html.twig',['comptes'=>$compteEtu]);
    }
    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/consult/notes",
     * name="consult_notes")
     */

    public function consult_notes(EntityManagerInterface $em)
    {
        $notes = $em->getRepository('App:Note')->findAll();
        $notesetu = array();


        $etudiants = $em->getRepository('App:Etudiant')->findAll();
        $compteEtu = array();





        foreach ($etudiants as $etudiant) {
            for($i=0;$i<1000;$i++) {
                if ($etudiant->getId() == $i){
                    array_push($compteEtu,$etudiant);
                    }
            }
        }


            foreach ($notes as $note) {
                for($i=0;$i<1000;$i++) {
    if( $note->getTestnoteNOTE() == $i) {
        array_push($notesetu, $note);
        }

    }

        }

        return $this->render('consult/consult_notes.html.twig', ['etudiants' => $compteEtu , 'notes' => $notesetu ]);
        }


    /**
     * @Route("/consult/etudiants/{id}", name="profiletu")
     */
    public function reactivateCompte(EntityManagerInterface $em, $id ){

        $moyenne=0;
        $totalnote=0;
        $dispo=0;
        $moyennetotale=0;
        $nombreetcs=0;

        $notes = $em->getRepository('App:Note')->findAll();
        $notesetu = array();

        $etudiants = $em->getRepository('App:Etudiant')->findAll();
        $compteEtu = array();

        $notesues = $em->getRepository('App:LiaisonNoteUe')->findAll();
        $compteNoteUe = array();

        $ues = $em->getRepository('App:Ue')->findAll();
        $compteUe = array();

        $etudiantsues = $em->getRepository('App:LiaisonUeEtudiant')->findAll();
        $compteEtudiantue = array();

//        $moyenne =Note::getTestnoteNOTE();

////        $infracRep = $em->getRepository('\App\Entity\Infraction');
//        $notesetu = $notes->getIdETUDIANT($compte->getIdETUDIANT());
//        if (count($notesetu == 1) ){
//            array_push($compteEtu,$no);
//        }
//

        foreach($etudiantsues as $etudiantue) {
//            if ($etudiantue->getIdETUDIANT() == $id) {
                array_push($compteEtudiantue, $etudiantue);
            }

       foreach($notes as $note) {
           if ($note->getTestnoteNOTE() == $id) {
               array_push($notesetu, $note);

           }
       }

           foreach ($ues as $ue) {
//              if ($ue->getIdUE() == $etudiantue->getIdETUDIANT()) {
                   array_push($compteUe, $ue);
                   $chiffre=1;

               }
//           }


        foreach($etudiants as $etudiant){

           if($etudiant->getIdETUDIANT()==$id){
            array_push($compteEtu,$etudiant);

          }
        }
        return $this->render('consult/consult_profil_etu.html.twig', ['nombreetcs'=>$nombreetcs,'moyennetotale'=>$moyennetotale,'dispo'=>$dispo,'moyenne'=>$moyenne,'totalnote'=>$totalnote,'etudiants' => $compteEtu , 'notes' => $notesetu ,  'ues'=> $compteUe ,  'etudiantsues'=> $compteEtudiantue] );

    }

    /**
     * @param EntityManagerInterface $em
     * @return Response
     *@Route("/consult/etudiants/{id}/ue", name="consult_ue_etu")
     */
    public function consult_ue_etu(EntityManagerInterface $em , $id)
    {
        $ues = $em->getRepository('App:Ue')->findAll();
        $compteUe = array();

        $etudiants = $em->getRepository('App:Etudiant')->findAll();
        $compteEtu = array();

        $etudiantsues = $em->getRepository('App:LiaisonUeEtudiant')->findAll();
        $compteEtuUe = array();

        foreach($etudiantsues as $etudiantue) {
                if ($etudiantue->getIdETUDIANT() == $id ) {
                    array_push($compteEtuUe, $etudiantue);

            }
        }
        foreach($ues as $ue) {
            array_push($compteUe, $ue);
        }
//        foreach($ues as $ue) {
//            for ($i = 0; $i < 1000; $i++) {
//                if ($ue->getIdUE() == $i) {
//
//                    array_push($compteUe, $ue);
//                }
//
//            }
//        }

        foreach($etudiants as $etudiant){

            if($etudiant->getIdETUDIANT()==$id){
                array_push($compteEtu,$etudiant);
            }
        }

        return $this->render('consult/consult_ue_etu.html.twig',['ues' => $compteUe,'etudiantsues' => $compteEtuUe,'etudiants' => $compteEtu] );
    }

    public function calculmoy(EntityManagerInterface $em)
    {
        $chiffre=0;
        return chiffre;
    }
}