<?php


namespace App\Controller;

use App\Form\GetLinkForRestType;
use App\Form\ResetPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Note;
use App\Entity\Etudiant;
use App\Repository\NoteRepository;

class UsefullController extends AbstractController
{
    /**
     * @return Response
     * @Route("listecompte",
     * name="liste_compte")
     */
    public function listecompte()
    {
        return $this->render('listecompte.html.twig');
    }
//    /**
//     * @Route("/laliste",
//     * name="laliste")
//     */
//    public function laliste()
//    {
//        return $this->render('listecompte.html.twig');
//    }

}