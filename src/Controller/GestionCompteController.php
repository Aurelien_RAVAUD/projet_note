<?php

namespace App\Controller;

use App\Form\GetLinkForRestType;
use App\Form\ResetPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class GestionCompteController extends AbstractController
{

    /**
     * @return Response
     * @Route("/gestion/compte",
     * name="gestion_compte")
     */
    public function gestionCompte()
    {
        return $this->render('gestion_compte/gestionCompte.html.twig');
    }



    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/admin",
     * name="gestion_compteAdmin")
     */
    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/admin",
     * name="gestion_compteAdmin")
     */
    public function gestionCompteAdmin(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:UtilisateurSite')->findAll();
        $compteAdmin = array();

        foreach($comptes as $compte){
            if($compte->getRole() == "admin" && $compte->getIsDesactivate() == false){
                array_push($compteAdmin,$compte);
            }
        }

        return $this->render('gestion_compte/compte.html.twig',['comptes'=>$compteAdmin]);
    }


    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/mention",
     * name="gestion_compteMention")
     */
    public function gestionCompteMention(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:UtilisateurSite')->findAll();
        $compteOpj = array();

        foreach($comptes as $compte){

            if($compte->getRole() == "mention" && $compte->getIsDesactivate() == false){
                array_push($compteOpj,$compte);
            }
        }

        return $this->render('gestion_compte/compte.html.twig',['comptes'=>$compteOpj]);
    }
    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/parcours",
     * name="gestion_compteParcours")
     */
    public function gestionCompteParcours(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:UtilisateurSite')->findAll();
        $compteUser = array();

        foreach($comptes as $compte){

            if($compte->getRole() == "parcours" && $compte->getIsDesactivate() == false){
                array_push($compteUser,$compte);
            }
        }

        return $this->render('gestion_compte/compte.html.twig',['comptes'=>$compteUser]);
    }
    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/Responsabledannee",
     * name="gestion_compteRannee")
     */
    public function gestionCompteRannee(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:UtilisateurSite')->findAll();
        $compteOpj = array();

        foreach($comptes as $compte){

            if($compte->getRole() == "rannee" && $compte->getIsDesactivate() == false){
                array_push($compteOpj,$compte);
            }
        }

        return $this->render('gestion_compte/compte.html.twig',['comptes'=>$compteOpj]);
    }
    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/enseignantdannee",
     * name="gestion_compteEannee")
     */
    public function gestionCompteEannee(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:UtilisateurSite')->findAll();
        $compteOpj = array();

        foreach($comptes as $compte){

            if($compte->getRole() == "eannee" && $compte->getIsDesactivate() == false){
                array_push($compteOpj,$compte);
            }
        }

        return $this->render('gestion_compte/compte.html.twig',['comptes'=>$compteOpj]);
    }
    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/ue",
     * name="gestion_compteUe")
     */
    public function gestionCompteUe(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:UtilisateurSite')->findAll();
        $compteOpj = array();

        foreach($comptes as $compte){

            if($compte->getRole() == "ue" && $compte->getIsDesactivate() == false){
                array_push($compteOpj,$compte);
            }
        }

        return $this->render('gestion_compte/compte.html.twig',['comptes'=>$compteOpj]);
    }


    /**
     * @param EntityManagerInterface $em
     * @return Response
     * @Route("/gestion/compte/desactive",
     * name="gestion_compteDesac")
     */
    public function gestionCompteDesactivate(EntityManagerInterface $em)
    {
        $comptes = $em->getRepository('App:UtilisateurSite')->findAll();
        $compteDesac = array();

        foreach($comptes as $compte){

            if($compte->getIsDesactivate() == true){
                array_push($compteDesac,$compte);
            }
        }

        return $this->render('gestion_compte/compte.html.twig',['comptes'=>$compteDesac]);
    }

    /**
     * @Route("/gestion/compte/desactive/{id}", name="deleteCompte")
     */
    public function desactivateCompte(EntityManagerInterface $em, $id){

        $compte = $em->getRepository('App:UtilisateurSite')->find($id);

        if(!$compte){
            throw new NotFoundHttpException("Le compte n'existe pas");
        }

        $compte->setIsDesactivate(true);
        $em->flush();

        return $this->redirectToRoute('gestion_compte');

    }

    /**
     * @Route("/gestion/compte/reactivate/{id}", name="reactivateCompte")
     */
    public function reactivateCompte(EntityManagerInterface $em, $id){

        $compte = $em->getRepository('App:UtilisateurSite')->find($id);

        if(!$compte){
            throw new NotFoundHttpException("Le compte n'existe pas");
        }

        $compte->setIsDesactivate(false);
        $em->flush();

        return $this->redirectToRoute('gestion_compte');

    }

    /**
     * @Route("/oubli", name="oublie")
     */
    public function forgotPassword(EntityManagerInterface $em,Request $req,\Swift_Mailer $mailer)
    {
        $form = $this->createForm(GetLinkForRestType::class);

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $dataUti = $req->request->get('get_link_for_rest');
            $uti = $em->getRepository('App:UtilisateurSite')->findByLogin($dataUti["login"]);
            $lien = base64_encode($uti->getLogin());
            $message = (new \Swift_Message('Récupération de compte'))
                ->setFrom('pvnovacompte@gmail.com')
                ->setTo($dataUti["login"])
                ->setBody(
                    $this->renderView(
                        'gestion_compte/forgotPassword.html.twig',
                        ["lien" => $lien]
                    ),
                    'text/html'
                );
            $mailer->send($message);
            return $this->redirectToRoute('accueil');

        } else {
            return $this->render(
                'gestion_compte/getLinkForReset.html.twig',
                ["form" => $form->createView()]
            );
        }

    }

    /**
     * @Route("/reset/{login}", name="reset")
     */
    public function resetPassword(UserPasswordEncoderInterface $encode,EntityManagerInterface $em,Request $req,$login)
    {
        $uti = $em->getRepository('App:UtilisateurSite')->findByLogin(base64_decode($login));
        $form = $this->createForm(ResetPasswordType::class);
        $form->add('reset', SubmitType::class, array('label' => 'Réinitialiser'));

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $dataUti = $req->request->get('reset_password');
            $encoded = $encode->encodePassword($uti, $dataUti["password"]["first"]);
            $uti->setPassword($encoded);
            $em->persist($uti);
            $em->flush();
            return $this->redirectToRoute('login');

        } else {
            return $this->render(
                'gestion_compte/getLinkForReset.html.twig',
                ["form" => $form->createView()]
            );
        }

    }
}
