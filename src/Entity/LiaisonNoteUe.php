<?php

namespace App\Entity;

use App\Repository\LiaisonNoteUeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LiaisonNoteUeRepository::class)
 */
class LiaisonNoteUe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Id_NOTE;

    /**
     * @ORM\Column(type="integer")
     */
    private $Id_UE;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdNOTE(): ?int
    {
        return $this->Id_NOTE;
    }

    public function setIdNOTE(int $Id_NOTE): self
    {
        $this->Id_NOTE = $Id_NOTE;

        return $this;
    }

    public function getIdUE(): ?int
    {
        return $this->Id_UE;
    }

    public function setIdUE(int $Id_UE): self
    {
        $this->Id_UE = $Id_UE;

        return $this;
    }
}
