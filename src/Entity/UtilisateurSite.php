<?php

namespace App\Entity;

use App\Repository\UtilisateurSiteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UtilisateurSiteRepository::class)
 */
class UtilisateurSite implements UserInterface, \Serializable {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $role;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDesactivate;

    public function getId(): ?int {
        return $this->id;
    }

    public function getLogin(): ?string {
        return $this->login;
    }

    public function getUsername(): ?string {
        return $this->login;
    }

    public function setLogin(string $login): self {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?string {
        return $this->role;
    }

    public function setRole(string $role): self {
        $this->role = $role;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDesactivate()
    {
        return $this->isDesactivate;
    }

    /**
     * @param mixed $isDesactivate
     */
    public function setIsDesactivate($isDesactivate): self
    {
        $this->isDesactivate = $isDesactivate;
        return $this;
    }


    /**
     * @return (Role|string)[] The user roles
     */
    public function getRoles() {
        $role = $this->getRole();

        switch ($role) {
            case "admin":
                return ['ROLE_ADMIN'];
                break;
            case "opj":
                return ['ROLE_OPJ'];
                break;
            case "usager":
                return ['ROLE_USER'];
                break;
            case "mention":
                return ['ROLE_MENTION'];
                break;
            case "rannee":
                return ['ROLE_RANNEE'];
                break;
            case "eannee":
                return ['ROLE_EANNEE'];
                break;
            case "parcours":
                return ['ROLE_PARCOURS'];

        }
    }

    public function getSalt() {
        return null;
    }

    public function eraseCredentials() {

    }

    public function serialize() {

        return serialize([
            $this->id,
            $this->login,
            $this->password
        ]);
    }

    public function unserialize($serialized) {

        list(
            $this->id,
            $this->login,
            $this->password
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

}
