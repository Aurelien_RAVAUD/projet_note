<?php

namespace App\Entity;

use App\Repository\UeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UeRepository::class)
 */
class Ue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $Id_UE;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_UE;

    /**
     * @ORM\Column(type="integer")
     */
    private $Etcs_UE;


    public function getIdUE(): ?int
    {
        return $this->Id_UE;
    }

    public function setIdUE(int $Id_UE): self
    {
        $this->Id_UE = $Id_UE;

        return $this;
    }

    public function getNomUE(): ?string
    {
        return $this->Nom_UE;
    }

    public function setNomUE(string $Nom_UE): self
    {
        $this->Nom_UE = $Nom_UE;

        return $this;
    }

    public function getEtcsUE(): ?int
    {
        return $this->Etcs_UE;
    }

    public function setEtcsUE(int $Etcs_UE): self
    {
        $this->Etcs_UE = $Etcs_UE;

        return $this;
    }
}
