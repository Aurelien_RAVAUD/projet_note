<?php

namespace App\Entity;

use App\Repository\LiaisonUeEtudiantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LiaisonUeEtudiantRepository::class)
 */
class LiaisonUeEtudiant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $Id_UE;




    /**
     * @ORM\Column(type="integer")
     */
    private $Id_ETUDIANT;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUE(): ?int
    {
        return $this->Id_UE;
    }

    public function setIdUE(int $Id_UE): self
    {
        $this->Id_UE = $Id_UE;

        return $this;
    }

    public function getIdETUDIANT(): ?int
    {
        return $this->Id_ETUDIANT;
    }

    public function setIdETUDIANT(int $Id_ETUDIANT): self
    {
        $this->Id_ETUDIANT = $Id_ETUDIANT;

        return $this;
    }
}
