<?php

namespace App\Entity;

use App\Repository\EtudiantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=EtudiantRepository::class)
 */
class Etudiant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $Id_ETUDIANT;

    public function __toString()
    {
        return (string) $this->Id_ETUDIANT;
    }



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenom_ETUDIANT;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_ETUDIANT;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="Id_ETUDIANT")
     */
    private $Note_ETUDIANT;

//    /**
//     * @ORM\Column(type="integer")
//     */
//    private $Id_ETUDIANT;

    public function __construct()
    {
        $this->Note_ETUDIANT = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->Id_ETUDIANT;
    }


    public function getPrenomETUDIANT(): ?string
    {
        return $this->Prenom_ETUDIANT;
    }

    public function setPrenomETUDIANT(string $Prenom_ETUDIANT): self
    {
        $this->Prenom_ETUDIANT = $Prenom_ETUDIANT;

        return $this;
    }

    public function getNomETUDIANT(): ?string
    {
        return $this->Nom_ETUDIANT;
    }

    public function setNomETUDIANT(string $Nom_ETUDIANT): self
    {
        $this->Nom_ETUDIANT = $Nom_ETUDIANT;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNoteETUDIANT(): Collection
    {
        return $this->Note_ETUDIANT;
    }

    public function addNoteETUDIANT(Note $noteETUDIANT): self
    {
        if (!$this->Note_ETUDIANT->contains($noteETUDIANT)) {
            $this->Note_ETUDIANT[] = $noteETUDIANT;
            $noteETUDIANT->setIdETUDIANT($this);
        }

        return $this;
    }

    public function removeNoteETUDIANT(Note $noteETUDIANT): self
    {
        if ($this->Note_ETUDIANT->removeElement($noteETUDIANT)) {
            // set the owning side to null (unless already changed)
            if ($noteETUDIANT->getIdETUDIANT() === $this) {
                $noteETUDIANT->setIdETUDIANT(null);
            }
        }

        return $this;
    }

    public function getIdETUDIANT(): ?int
    {
        return $this->Id_ETUDIANT;
    }

    public function setIdETUDIANT(int $Id_ETUDIANT): self
    {
        $this->Id_ETUDIANT = $Id_ETUDIANT;

        return $this;
    }

}
