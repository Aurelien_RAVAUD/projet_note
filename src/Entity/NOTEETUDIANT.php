<?php

namespace App\Entity;

use App\Repository\NOTEETUDIANTRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NOTEETUDIANTRepository::class)
 */
class NOTEETUDIANT
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Id_NOTEETUDIANT;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdNOTEETUDIANT(): ?string
    {
        return $this->Id_NOTEETUDIANT;
    }

    public function setIdNOTEETUDIANT(string $Id_NOTEETUDIANT): self
    {
        $this->Id_NOTEETUDIANT = $Id_NOTEETUDIANT;

        return $this;
    }
}
