<?php

namespace App\Entity;

use App\Repository\ParametreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParametreRepository::class)
 */
class Parametre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $desc_parametre;

    /**
     * @ORM\OneToMany(targetEntity=Prends::class, mappedBy="parametre")
     */
    private $prends;

    /**
     * @ORM\OneToMany(targetEntity=Determine::class, mappedBy="parametre")
     */
    private $determine;

    public function __construct()
    {
        $this->prends = new ArrayCollection();
        $this->determine = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescParametre(): ?string
    {
        return $this->desc_parametre;
    }

    public function setDescParametre(string $desc_parametre): self
    {
        $this->desc_parametre = $desc_parametre;

        return $this;
    }

    /**
     * @return Collection|Prends[]
     */
    public function getPrends(): Collection
    {
        return $this->prends;
    }

    public function addPrend(Prends $prend): self
    {
        if (!$this->prends->contains($prend)) {
            $this->prends[] = $prend;
            $prend->setParametre($this);
        }

        return $this;
    }

    public function removePrend(Prends $prend): self
    {
        if ($this->prends->removeElement($prend)) {
            // set the owning side to null (unless already changed)
            if ($prend->getParametre() === $this) {
                $prend->setParametre(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Determine[]
     */
    public function getDetermine(): Collection
    {
        return $this->determine;
    }

    public function addDetermine(Determine $determine): self
    {
        if (!$this->determine->contains($determine)) {
            $this->determine[] = $determine;
            $determine->setParametre($this);
        }

        return $this;
    }

    public function removeDetermine(Determine $determine): self
    {
        if ($this->determine->removeElement($determine)) {
            // set the owning side to null (unless already changed)
            if ($determine->getParametre() === $this) {
                $determine->setParametre(null);
            }
        }

        return $this;
    }
}
