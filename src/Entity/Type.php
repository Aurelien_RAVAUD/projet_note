<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 */
class Type
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $desc_type;

    /**
     * @ORM\OneToMany(targetEntity=Determine::class, mappedBy="type")
     */
    private $determine;

    /**
     * @ORM\OneToMany(targetEntity=Possede::class, mappedBy="type")
     */
    private $possede;


    public function __construct()
    {
        $this->parametre = new ArrayCollection();
        $this->determine = new ArrayCollection();
        $this->possede = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescType(): ?string
    {
        return $this->desc_type;
    }

    public function setDescType(string $desc_type): self
    {
        $this->desc_type = $desc_type;

        return $this;
    }

    /**
     * @return Collection|Determine[]
     */
    public function getDetermine(): Collection
    {
        return $this->determine;
    }

    public function addDetermine(Determine $determine): self
    {
        if (!$this->determine->contains($determine)) {
            $this->determine[] = $determine;
            $determine->setType($this);
        }

        return $this;
    }

    public function removeDetermine(Determine $determine): self
    {
        if ($this->determine->removeElement($determine)) {
            // set the owning side to null (unless already changed)
            if ($determine->getType() === $this) {
                $determine->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Possede[]
     */
    public function getPossede(): Collection
    {
        return $this->possede;
    }

    public function addPossede(Possede $possede): self
    {
        if (!$this->possede->contains($possede)) {
            $this->possede[] = $possede;
            $possede->setType($this);
        }

        return $this;
    }

    public function removePossede(Possede $possede): self
    {
        if ($this->possede->removeElement($possede)) {
            // set the owning side to null (unless already changed)
            if ($possede->getType() === $this) {
                $possede->setType(null);
            }
        }

        return $this;
    }
}
