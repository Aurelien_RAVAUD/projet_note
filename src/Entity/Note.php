<?php

namespace App\Entity;

use App\Repository\NoteRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 */
class Note
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $Id_NOTE;

//    /**
//     * @ORM\Column(type="integer")
//     */
//    private $Id_NOTE;

    /**
     * @ORM\Column(type="float")
     */
    private $Note_NOTE;

//    /**@ORM\Column(type="integer")
//     * @ORM\ManyToOne(targetEntity=Etudiant::class, inversedBy="Note_ETUDIANT")
//     * // @ORM\JoinColumn(name="IdETUDIANT", referencedColumnName="Id_ETUDIANT")
//     */
//    private $Id_ETUDIANT;

    /**
     * @ORM\Column(type="integer")
     */
    private $Testnote_NOTE;

    /**
     * @ORM\Column(type="integer")
     */
    private $Id_UE;

    /**
     * @ORM\Column(type="integer")
     */
    private $Coeff_NOTE;

//    public function getId(): ?int
//    {
//        return $this->id;
//    }

    public function getIdNOTE(): ?int
    {
        return $this->Id_NOTE;
    }

    public function setIdNOTE(int $Id_NOTE): self
    {
        $this->Id_NOTE = $Id_NOTE;

        return $this;
    }

    public function getNoteNOTE()//: ?float
    {
        return $this->Note_NOTE;
    }

    public function setNoteNOTE(float $Note_NOTE): self
    {
        $this->Note_NOTE = $Note_NOTE;

        return $this;
    }



//    public function getIdETUDIANT()
//    {
//        return $this->Id_ETUDIANT;
//    }
//
//    public function setIdETUDIANT(Etudiant  $Id_ETUDIANT): self
//    {
//        $this->Id_ETUDIANT = $Id_ETUDIANT;
//
//        return $this;
//    }

public function getTHENOTE(): ?object{
        return $this->TestnoteNOTE();
}

    public function getTestnoteNOTE(): ?int
    {
        return $this->Testnote_NOTE;
    }

    public function setTestnoteNOTE(int $Testnote_NOTE): self
    {
        $this->Testnote_NOTE = $Testnote_NOTE;

        return $this;
    }

    public function getIdUE(): ?int
    {
        return $this->Id_UE;
    }

    public function setIdUE(int $Id_UE): self
    {
        $this->Id_UE = $Id_UE;

        return $this;
    }

    public function getCoeffNOTE(): ?int
    {
        return $this->Coeff_NOTE;
    }

    public function setCoeffNOTE(int $Coeff_NOTE): self
    {
        $this->Coeff_NOTE = $Coeff_NOTE;

        return $this;
    }
}
