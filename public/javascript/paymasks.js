$(document).ready(function() {
    $("#payer_cardNumber").inputmask("9999-9999-9999-9999");
    $("#payer_cryptogramme").inputmask("999");

    $("#payer_expirationDate_month").addClass("custom-select");
    $("#payer_expirationDate_year").addClass("custom-select");
})